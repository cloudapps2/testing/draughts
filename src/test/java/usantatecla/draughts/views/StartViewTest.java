package usantatecla.draughts.views;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import usantatecla.draughts.controllers.StartController;
import usantatecla.draughts.models.Coordinate;

import static org.mockito.Mockito.*;

public class StartViewTest {

    @InjectMocks
    private StartView startView;

    @Mock
    private StartController startController;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGivenAStartControllerInteract() {
        when(this.startController.getDimension()).thenReturn(8);
        this.startView.interact(this.startController);

        verify(this.startController).start();
        verify(this.startController, atLeast(1)).getDimension();
        verify(this.startController, times(73)).getDimension();
        verify(this.startController, atLeast(1)).getPiece(any(Coordinate.class));
        verify(this.startController, times(64)).getPiece(any(Coordinate.class));
    }

    @Test(expected = AssertionError.class)
    public void testGivenNullStartControllerWhenInteractThenThrowException() {
        this.startView.interact(null);
    }
}