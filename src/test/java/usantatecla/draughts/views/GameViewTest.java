package usantatecla.draughts.views;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import usantatecla.draughts.controllers.InteractorController;
import usantatecla.draughts.models.*;

import static org.mockito.ArgumentMatchers.*;

import static org.mockito.Mockito.when;

import static org.junit.Assert.*;

public class GameViewTest {

    @InjectMocks
    private GameView gameView;

    @Mock
    private InteractorController controller;

    @Mock
    private Piece piece;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGivenInteractorControllerWhenWriteWithPieceNull() {
        when(controller.getDimension()).thenReturn(1);
        this.gameView.write(this.controller);
    }

    @Test
    public void testGivenInteractorControllerWhenWriteWithPiece() {
        when(controller.getDimension()).thenReturn(8);
        when(controller.getPiece(any(Coordinate.class))).thenReturn(this.piece);
        when(this.piece.getCode()).thenReturn("n").thenReturn(" ").thenReturn("n").thenReturn(" ");
        this.gameView.write(this.controller);
    }

    @Test(expected = AssertionError.class)
    public void testGivenNullControllerThenThrowException() {
        this.gameView.write(null);
    }
}