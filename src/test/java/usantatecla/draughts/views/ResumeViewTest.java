package usantatecla.draughts.views;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import usantatecla.draughts.controllers.ResumeController;
import usantatecla.draughts.utils.YesNoDialog;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class ResumeViewTest {

    @InjectMocks
    private ResumeView resumeView;

    @Mock
    private ResumeController resumeController;

    @Mock
    private YesNoDialog yesNoDialog;

    @Before
    public void before(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGivenYesWhenInteract() {
        when(this.yesNoDialog.read(anyString())).thenReturn(true);

        this.resumeView.interact(this.resumeController);

        verify(this.yesNoDialog, atLeast(1)).read(anyString());
        verify(this.resumeController).reset();
        verify(this.resumeController, never()).next();
    }

    @Test
    public void testGivenNoWhenInteract() {
        when(this.yesNoDialog.read(anyString())).thenReturn(false);

        this.resumeView.interact(this.resumeController);

        verify(this.yesNoDialog, atLeast(1)).read(anyString());
        verify(this.resumeController, never()).reset();
        verify(this.resumeController).next();
    }

    @Test(expected = AssertionError.class)
    public void testGivenNullControllerWhenInteractThenThrowException(){
        this.resumeView.interact(null);
    }
}