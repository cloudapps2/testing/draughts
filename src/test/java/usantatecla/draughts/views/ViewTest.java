package usantatecla.draughts.views;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import usantatecla.draughts.controllers.InteractorController;
import usantatecla.draughts.controllers.PlayController;
import usantatecla.draughts.controllers.ResumeController;
import usantatecla.draughts.controllers.StartController;

public class ViewTest {

    @InjectMocks
    private View view;

    @Mock
    private StartView startView;

    @Mock
    private PlayView playView;

    @Mock
    private ResumeView resumeView;

    @Mock
    private StartController startController;

    @Mock
    private PlayController playController;

    @Mock
    private ResumeController resumeController;

    @Mock
    private InteractorController interactorController;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGivenStartControllerWhenVisit() {
        this.view.visit(this.startController);
    }

    @Test(expected = AssertionError.class)
    public void testGivenNullStartControllerWhenVisit() {
        this.view.visit((StartController) null);
    }

    @Test
    public void testGivenPlayControllerWhenVisit() {
        this.view.visit(this.playController);
    }

    @Test(expected = AssertionError.class)
    public void testGivenNullPlayControllerWhenVisit() {
        this.view.visit((PlayController) null);
    }

    @Test
    public void testGivenResumeControllerWhenVisit(){
        this.view.visit(this.resumeController);
    }

    @Test(expected = AssertionError.class)
    public void testGivenNullResumeControllerWhenVisit() {
        this.view.visit((ResumeController) null);
    }

    @Test
    public void testGivenInteractorControllerWhenInteract() {
        this.view.interact(this.interactorController);
    }

    @Test(expected = AssertionError.class)
    public void testGivenNullControllerWhenInteract() {
        this.view.interact(null);
    }
}