package usantatecla.draughts.views;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import usantatecla.draughts.controllers.PlayController;
import usantatecla.draughts.models.Color;
import usantatecla.draughts.models.Coordinate;
import usantatecla.draughts.models.Error;
import usantatecla.draughts.models.Piece;
import usantatecla.draughts.utils.Console;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class PlayViewTest {

    private static final String PROMPT = "Mueven las blancas: ";

    @InjectMocks
    private PlayView playView;
    @Mock
    private PlayController playController;
    @Mock
    private Console console;
    @Mock
    private Piece piece;

    @Before
    public void before(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGivenACorrectMovementWhenInteract(){
        when(this.playController.getColor()).thenReturn(Color.WHITE);
        when(this.console.readString(PROMPT)).thenReturn("61.52");
        when(this.playController.getDimension()).thenReturn(8);
        when(this.playController.getPiece(new Coordinate(5,2))).thenReturn(this.piece);
        when(this.piece.getCode()).thenReturn("b");

        this.playView.interact(this.playController);

        verify(this.playController).getColor();
        verify(this.console).readString(anyString());
        verify(this.playController).move(any(Coordinate.class));
        verify(this.playController, atLeast(1)).getDimension();
        verify(this.playController, times(73)).getDimension();
        verify(this.playController, atLeast(1)).getPiece(any(Coordinate.class));
        verify(this.playController, times(64)).getPiece(any(Coordinate.class));
    }

    @Test
    public void testGivenCanceledFormatWhenInteract(){
        when(this.playController.getColor()).thenReturn(Color.WHITE);
        when(this.console.readString(PROMPT)).thenReturn("-1");

        this.playView.interact(this.playController);

        verify(this.playController).getColor();
        verify(this.console).readString(anyString());
        verify(this.playController).cancel();
    }

    @Test
    public void testGivenIncorrectFormatWhenInteract(){
        when(this.playController.getColor()).thenReturn(Color.WHITE);
        when(this.console.readString(PROMPT)).thenReturn("11,22").thenReturn("61.52");
        this.playView.interact(this.playController);

        verify(this.playController, times(2)).getColor();
        verify(this.playController, atMost(2)).getColor();
        verify(this.console , times(2)).readString(anyString());
        verify(this.console, atMost(2)).readString(anyString());
    }

    @Test
    public void testGivenIncorrectMovementWhenInteract(){
        when(this.playController.getColor()).thenReturn(Color.WHITE);
        when(this.playController.move(any(Coordinate.class))).thenReturn(null);
        when(this.playController.isBlocked()).thenReturn(true);
        when(this.console.readString(PROMPT)).thenReturn("61.52");
        this.playView.interact(this.playController);
    }

    @Test(expected = AssertionError.class)
    public void testGivenNullControllerWhenInteractThenThrowException(){
        this.playView.interact(null);
    }
}