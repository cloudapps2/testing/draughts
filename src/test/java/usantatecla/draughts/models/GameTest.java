package usantatecla.draughts.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GameTest {
	
	private GameBuilder gameBuilder;
	
	@Before
	public void before() {
		gameBuilder = new GameBuilder();
	}

	@Test
	public void testWhenMoveCorrect() {
		String[] rows = {
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"b       "};
		Game game = this.gameBuilder.build(rows);
		assertNull(game.move(createCoordinates(7,0), createCoordinates(6,1)));
	}
	
	@Test
	public void testGivenEmptyOriginWhenMoveThenReturnEmptyOrigin() {
		String[] rows = {
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        "};
		Game game = this.gameBuilder.build(rows);
		assertEquals(Error.EMPTY_ORIGIN, game.move(createCoordinates(0,0), createCoordinates(1,1)));
	}
	
	@Test
	public void testGivenNotEmptyTargetWhenMoveThenReturnNotEmptyTarget() {
		String[] rows = {
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				" b      ",
				"b       "};
		Game game = this.gameBuilder.build(rows);
		assertEquals(Error.NOT_EMPTY_TARGET, game.move(createCoordinates(7,0), createCoordinates(6,1)));
	}
	
	private Coordinate createCoordinates(int row, int col) {
		return new Coordinate(row, col);
	}
	
	@Test
	public void testGivenOpositePieceWhenMoveThenReturnOppositeMove() {
		String[] rows = {
				"n       ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"b       "};
		Game game = this.gameBuilder.build(rows);
		assertEquals(Error.OPPOSITE_PIECE, game.move(createCoordinates(0,0), createCoordinates(0,0)));
	}
	
	@Test
	public void testWhenMoveCorrectThenRemoveOpposite() {
		String[] rows = {
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				" n      ",
				"b       "};
		Game game = this.gameBuilder.build(rows);
		assertNull(game.move(createCoordinates(7,0), createCoordinates(5,2)));
	}
	
	@Test
	public void testWhenMoveCorrectThenDraught() {
		String[] rows = {
				"        ",
				"  n     ",
				" b      ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        "};
		Game game = this.gameBuilder.build(rows);
		assertNull(game.move(createCoordinates(2,1), createCoordinates(0,3)));
	}
	
	@Test
	public void testIsBlockedThenIsFalse() {
		String[] rows = {
				"        ",
				"  n     ",
				" b      ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        "};
		Game game = this.gameBuilder.build(rows);
		assertFalse(game.isBlocked());
	}
	
	@Test
	public void testIsBlockedThenIsTrue() {
		String[] rows = {
				"   n    ",
				"n n     ",
				" b      ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        "};
		Game game = this.gameBuilder.build(rows);
		assertTrue(game.isBlocked());
	}
	
	@Test
	public void testGetColor() {
		String[] rows = {
				"        ",
				"n n     ",
				" b      ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        "};
		Game game = this.gameBuilder.build(rows);
		assertEquals(Color.WHITE, game.getColor(Coordinate.getInstance("32")));
		assertEquals(Color.BLACK, game.getColor(Coordinate.getInstance("21")));
		assertEquals(Color.BLACK, game.getColor(Coordinate.getInstance("23")));
	}
	
	@Test(expected = AssertionError.class)
	public void testGivenNullCoordinateWhenGetColorThenThrowException() {
		String[] rows = {
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        ",
				"        "};
		Game game = this.gameBuilder.build(rows);
		assertEquals(Color.WHITE, game.getColor(null));
	}

}
