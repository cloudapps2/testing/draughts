package usantatecla.draughts.models;

public class GameBuilder {
	
	public Game build(String... rows) {
		Board board = new Board();
		assert rows.length == Coordinate.getDimension();
		for(int i = 0; i < rows.length; i++) {
			String row = rows[i];
			for(int j = 0; j < row.length(); j++) {
				Color color = color(row.charAt(j));
				if(color != null) {
					Piece piece;
					if(Character.isUpperCase(row.charAt(j))) {
						piece = new Draught(color);
					} else {
						piece = new Pawn(color);
					}
					board.put(new Coordinate(i, j), piece);
				}
			}
			
		}
		return new Game(board);
	}

	private Color color(char color) {
		switch (color) {
		case 'b':
		case 'B':	
			return Color.WHITE;
		case 'n':
		case 'N':	
			return Color.BLACK;
		default:
			break;
		}
		return null;
	}
}
