package usantatecla.draughts.models;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

public class PieceTest {
	
	private Piece pieceWhite;
	
	private Piece pieceBlack;
	
	private Piece draught;

	private Coordinate[] coordinates = new Coordinate[2];
	
	@Before
	public void before() {
		this.pieceWhite = new Pawn(Color.WHITE);
		this.pieceBlack = new Pawn(Color.BLACK);
		this.draught = new Draught(Color.WHITE);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGivenWhitePawnWithC07WhenIsLimitThenIsTrue() {
		assertEquals(true, this.pieceWhite.isLimit(Coordinate.getInstance("18")));
	}
	
	@Test
	public void testGivenBlackPawnWithC70WhenIsLimitThenIsTrue() {
		assertEquals(true, this.pieceBlack.isLimit(Coordinate.getInstance("81")));
	}
	
	@Test
	public void testGivenWhitePawnWith17WhenIsLimitThenIsFalse() {
		assertEquals(false, this.pieceWhite.isLimit(Coordinate.getInstance("28")));
	}
	
	@Test
	public void testGivenBlackPawnkWith67WhenIsLimitThenIsFalse() {
		assertEquals(false, this.pieceBlack.isLimit(Coordinate.getInstance("71")));
	}
	
	@Test
	public void testGivenWhitePawnWithC77WhenIsCorrectMovementToC66ThenIsTrue() {
		assertEquals(true, this.pieceWhite.isAdvanced(Coordinate.getInstance("88"), 
				Coordinate.getInstance("77")));
	}
	
	@Test
	public void testGivenWhitePawnWithC00WhenIsCorrectMovementToC22ThenIsFalse() {
		assertEquals(false, this.pieceWhite.isAdvanced(Coordinate.getInstance("11"), 
				Coordinate.getInstance("33")));
	}
	
	@Test
	public void testGivenBlackPawnWithC00WhenIsCorrectMovementToC11ThenIsTrue() {
		assertEquals(true, this.pieceBlack.isAdvanced(Coordinate.getInstance("11"), 
				Coordinate.getInstance("22")));
	}
	
	@Test
	public void testGivenBlackPawnWithC33WhenIsCorrectMovementToC22ThenIsFalse() {
		assertEquals(false, this.pieceBlack.isAdvanced(Coordinate.getInstance("44"), 
				Coordinate.getInstance("33")));
	}

	@Test(expected = AssertionError.class)
	public void testGivenNullOriginWhenIsCorrectMovementThenThrowException() {
		this.pieceBlack.isAdvanced(null, Coordinate.getInstance("33"));
	}
	
	@Test(expected = AssertionError.class)
	public void testGivenNullTargetWhenIsCorrectMovementThenThrowException() {
		this.pieceBlack.isAdvanced(Coordinate.getInstance("33"), null);
	}
	
	@Test
	public void givenPawnWhenIsMovement() {
		coordinates[0] = Coordinate.getInstance("61");
		coordinates[1] = Coordinate.getInstance("52");
		assertEquals(null, this.pieceWhite.isCorrectMovement(Arrays.asList(new Pawn(Color.BLACK)), 0, coordinates));
	}
	
	@Test
	public void givenPawnWhenIsCorrectMovementAndColorThenReturnError() {
		coordinates[0] = Coordinate.getInstance("61");
		coordinates[1] = Coordinate.getInstance("52");
		assertEquals(Error.COLLEAGUE_EATING, this.pieceWhite.isCorrectMovement(Arrays.asList(new Pawn(Color.WHITE)), 0, coordinates));
	}
	
	@Test
	public void givenPawnWhenIsCorrectMovementWithNotDiagonalThenReturnError() {
		coordinates[0] = Coordinate.getInstance("61");
		coordinates[1] = Coordinate.getInstance("53");
		assertEquals(Error.NOT_DIAGONAL, this.pieceWhite.isCorrectMovement(Arrays.asList(new Pawn(Color.WHITE)), 0, coordinates));
	}
	
	@Test
	public void givenPawnWhenIsCorrectDiagonalMovementSameCoordinateThenReturnNotAdvance() {
		coordinates[0] = Coordinate.getInstance("47");
		coordinates[1] = Coordinate.getInstance("58");
		assertEquals(Error.NOT_ADVANCED, this.pieceWhite.isCorrectMovement(Arrays.asList(new Pawn(Color.BLACK)), 0, coordinates));
	}
	
	@Test
	public void givenPawnWhenIsCorrectDiagonalMovementMaxDistanceThenReuturnTooMuchadvanced() {
		coordinates[0] = Coordinate.getInstance("88");
		coordinates[1] = Coordinate.getInstance("11");
		assertEquals(Error.TOO_MUCH_ADVANCED, this.pieceWhite.isCorrectMovement(Arrays.asList(new Pawn(Color.BLACK)), 0, coordinates));
	}
	
	@Test
	public void givenPawnWhenIsCorrectDiagonalMovementAmountBetweenDiagonalPiecesNot1ThenReturnWithoutEating() {
		coordinates[0] = Coordinate.getInstance("61");
		coordinates[1] = Coordinate.getInstance("43");
		assertEquals(Error.WITHOUT_EATING, this.pieceWhite.isCorrectMovement(Arrays.asList(), 0, coordinates));
	}
	
	@Test
	public void testGivenDraughtWhenIsCorrectMovement() {
		coordinates[0] = Coordinate.getInstance("47");
		coordinates[1] = Coordinate.getInstance("65");
		assertEquals(null, this.draught
				.isCorrectMovement(Arrays.asList(new Pawn(Color.BLACK)), 0, coordinates));
	}
	
	@Test
	public void givenDraughtWhenIsCorrectMovementAndAmountIsBigThenReturnErrorTooMuchEatings() {
		coordinates[0] = Coordinate.getInstance("47");
		coordinates[1] = Coordinate.getInstance("65");
		assertEquals(Error.TOO_MUCH_EATINGS, this.draught
				.isCorrectMovement(Arrays.asList(new Pawn(Color.BLACK), new Pawn(Color.BLACK)), 0, coordinates));
	}
}
