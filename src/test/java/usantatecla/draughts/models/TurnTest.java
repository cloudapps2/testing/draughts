package usantatecla.draughts.models;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TurnTest {
	
	private Turn turn;
	
	@Before
	public void before() {
		this.turn = new Turn();
	}

	@Test
	public void givenNewTurnWhenGetColorTurnThenIsWhite() {
		assertEquals(Color.WHITE, this.turn.getColor());
	}
	
	@Test 
	public void givenNewTurnWhenGetOppositeColorTurnThenIsBlack() {
		assertEquals(Color.BLACK, this.turn.getOppositeColor());
	}
	
	@Test
	public void givenNewTurnWhenCompareTurnWithTurnCopyThenIsTrue() {
		Turn turnCopy = new Turn();
		assertTrue(this.turn.equals(turnCopy));
	}
	
	@Test
	public void givenNewTurnWhenCompareTurnWithDifferentColorThenIsFalse() {
		Turn turn = new Turn();
		turn.change();
		assertFalse(this.turn.equals(turn));
	}
	
	@Test
	public void givenNewTurnWhenComparesWithObjectThenIsFalse() {
		assertFalse(this.turn.equals(new Object()));
	}
	
	@Test
	public void givenNewTurnWhenCompareWithSameTurnThenIsTrue() {
		assertTrue(this.turn.equals(this.turn));
	}
	
	@Test
	public void givenNewTurnWhenCompareWithNullObjectThenIsFalse() {
		assertFalse(this.turn.equals(null));
	}

}
