package usantatecla.draughts.models;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BoardTest {
	
	private Board board;
	private Piece piece;
	private Coordinate coordinate01;
	
	
	@Before
	public void before() {
		this.board = new Board();
		this.coordinate01 = new Coordinate(0,1);
		this.piece = new Pawn(Color.getInitialColor(this.coordinate01));
	}
	
	@Test
	public void testGivenNewBoardWhenPutNewPieceIsEmptyThenIsTrue() {
		this.board.put(this.coordinate01, this.piece);
		assertFalse(this.board.isEmpty(this.coordinate01));
	}

	@Test(expected = AssertionError.class)
	public void testGivenNewBoardWhenPutNewPieceWithNullCordinateThenThrowException() {
		this.board.isEmpty(null);
	}
	
	@Test
	public void testGivenNewBoardWhenIsEmptyWithEmptyCoordinateThenIsTrue() {
		Coordinate coordinate = new Coordinate(0,0);
		assertTrue(this.board.isEmpty(coordinate));
	}

	

}
