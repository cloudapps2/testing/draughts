package usantatecla.draughts.models;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CoordinateTest {

	private Coordinate coordinate00;
	private Coordinate coordinate11;
	private Coordinate coordinate02;
	private Coordinate coordinate20;
	private Coordinate coordinate22;
	private Coordinate coordinate33;

	@Before
	public void before() {
		this.coordinate00 = new Coordinate(0, 0);
		this.coordinate11 = new Coordinate(1, 1);
		this.coordinate02 = new Coordinate(0, 2);
		this.coordinate20 = new Coordinate(2, 0);
		this.coordinate22 = new Coordinate(2, 2);
		this.coordinate33 = new Coordinate(3, 3);
	}

	@Test
	public void testGivenCoordinate11WhenGetDirectionWithC00ThenIsSW() {
		assertEquals(Direction.SW, coordinate11.getDirection(coordinate00));
	}

	@Test
	public void testGivenCoordinate11WhenGetDirectionWithC02ThenIsSE() {
		assertEquals(Direction.SE, coordinate11.getDirection(coordinate02));
	}

	@Test
	public void testGivenCoordinate11WhenGetDirectionWithC20ThenIsNW() {
		assertEquals(Direction.NW, coordinate11.getDirection(coordinate20));
	}

	@Test
	public void testGivenCoordinate11WhenGetDirectionWithC22ThenIsNE() {
		assertEquals(Direction.NE, coordinate11.getDirection(coordinate22));
	}

	@Test
	public void testgivenCoordinate00WhenGetDirectionWithC20ThenIsNull() {
		assertEquals(null, this.coordinate00.getDirection(coordinate20));
	}
	
	@Test(expected = AssertionError.class)
	public void testGivenCoordinateWhenGetDirectionWithNullThenThrowException() {
		this.coordinate00.getDirection(null);
	}
	
	@Test
	public void testGivenCoordinate11WhenGetDiagonalDistanceWithC00ThenIs1() {
		assertEquals(1, this.coordinate11.getDiagonalDistance(this.coordinate00));
	}
	
	@Test(expected = AssertionError.class)
	public void testGivenCoordinateWhenGetDiagonalDistanceWithNotDiagonalThenThrowException() {
		this.coordinate00.getDiagonalDistance(this.coordinate20);
	}
	
	@Test(expected = AssertionError.class)
	public void testGivenCoordinate11WhenGetDiagonalWithNullThen() {
		assertEquals(1, this.coordinate11.getDiagonalDistance(null));
	}
	
	@Test
	public void testGivenCoordinate11WhenGetBetweenDiagonalCoordinateWith00ThenIsCoordinate11() {
		assertEquals(this.coordinate11, this.coordinate22.getBetweenDiagonalCoordinate(this.coordinate00));
	}
	
	@Test(expected = AssertionError.class)
	public void testGivenCoordinate11WhenGetBetweenDiagonalCoordinateWith00ThenThrowException() {
		this.coordinate11.getBetweenDiagonalCoordinate(this.coordinate00);
	}
	
	@Test
	public void givenNewCoordinateWhenGetBetweenDiagonalCoordinatesCoordinate00and33ThenReturnCoordinates() {
		List<Coordinate> betweenDiagonalCoordinates = new ArrayList<>();
		betweenDiagonalCoordinates.add(this.coordinate11);
		betweenDiagonalCoordinates.add(this.coordinate22);
		assertEquals(betweenDiagonalCoordinates, this.coordinate00.getBetweenDiagonalCoordinates(coordinate33));
	}
	
	@Test
	public void testGivenCoordinate00WhenGetDiagonalCoordinateThenReturnBetweeDiagonalCoordinate() {
		List<Coordinate> betweenDiagonalCoordinates = new ArrayList<>();
		betweenDiagonalCoordinates.add(this.coordinate11);
		assertEquals(betweenDiagonalCoordinates, this.coordinate00.getDiagonalCoordinates(1));
	}
	
	@Test
	public void testGivenCoordinateNotWithinWhenGetDiagonalCoordinateThenReturnEmptyList() {
		Coordinate coordinate = new Coordinate(9, 9);
		assertEquals(new ArrayList<>() ,coordinate.getDiagonalCoordinates(1));
	}
	
	@Test
	public void testGivenACoordenate88WhenIsLastThenIsTrue() {
		assertEquals(Coordinate.getInstance("88").isLast(), true);
	}
	
	@Test
	public void testGivenCoordinate11WhenIsBlackThenIsTrue() {
		assertThat(Coordinate.getInstance("12").isBlack(), is(Boolean.TRUE));
	}
	
	@Test
	public void testGivenCoordinate11WhenIsBlackThenIsFalse() {
		assertThat(this.coordinate11.isBlack(), is(Boolean.FALSE));
	}
	
	@Test
	public void testGivenACoordinate11WhenIsLastThenIsFalse() {
		assertEquals(this.coordinate11.isLast(), false);
	}
	
	@Test
	public void testGivenACoordinate00WhenIsFirstThenIsTrue() {
		assertEquals(this.coordinate00.isFirst(), true);
	}
	
	@Test
	public void testGivenACoordinate11WhenIsFirstThenIsFalse() {
		assertEquals(this.coordinate11.isFirst(), false);
	}

	@Test
	public void testGivenCoordinateWhenCompareTwoCoordinatesEqualsThenIsTrue() {
		Coordinate coordinate00Copy = new Coordinate(0, 0);
		assertTrue(this.coordinate00.equals(coordinate00Copy));
	}
	
	@Test
	public void testGivenCoordinateWhenComparesWithNullThenIsFalse() {
		assertFalse(this.coordinate00.equals(null));
	}
	
	@Test
	public void testGivenCoordinateWhenComparesWithSameCoordinateThenIsTrue() {
		assertTrue(this.coordinate00.equals(this.coordinate00));
	}

	@Test
	public void testGivenCoordinatesWhenCompareTwoCoordinateNotEqualsRowThenIsTrue() {
		Coordinate coordinate01Copy = new Coordinate(1, 0);
		assertTrue(!this.coordinate00.equals(coordinate01Copy));
	}

	@Test
	public void testGivenCoordinatesWhenCompareTwoCoordinateNotEqualsColumnThenIsTrue() {
		Coordinate coordinate01Copy = new Coordinate(0, 1);
		assertTrue(!this.coordinate00.equals(coordinate01Copy));
	}

	@Test
	public void testGivenCoordinatesWhenCompareOneCoordinateWithAnObjectThenIsFalse() {
		assertFalse(this.coordinate00.equals(new Object()));
	}

	@Test
	public void testGivenCoordinateWithRowNotWithinWhenGetInstanceThenReturnNull() {
		assertEquals(null, Coordinate.getInstance("01"));
		assertEquals(null, Coordinate.getInstance("91"));
	}
	
	@Test
	public void testGivenCoordinateWithColNotWithinWhenGetInstanceThenReturnNull() {
		assertEquals(null, Coordinate.getInstance("99"));
		assertEquals(null, Coordinate.getInstance("90"));
	}
}
