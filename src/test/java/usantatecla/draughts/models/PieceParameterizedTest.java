package usantatecla.draughts.models;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class PieceParameterizedTest {
	
	private Piece piece;
	
	private List<Piece> betweenDiagonalPieces;
	private int pair;
	private Coordinate[] coordinates;
	private Error expectedError;
	
	private Coordinate coordinateLimit;
	private boolean isLimit;
	
	private Coordinate coordinateOrigin;
	private Coordinate coordinateTarget;
	private boolean isAdvanced;
	
	public PieceParameterizedTest(Piece piece, List<Piece> betweenDiagonalPieces, int pair, Coordinate[] coordinates,
			Error expectedError, Coordinate coordinateLimit, boolean isLimit, Coordinate coordinateOrigin,
			Coordinate coordinateTarget, boolean isAdvanced) {
		super();
		this.piece = piece;
		this.betweenDiagonalPieces = betweenDiagonalPieces;
		this.pair = pair;
		this.coordinates = coordinates;
		this.expectedError = expectedError;
		this.coordinateLimit = coordinateLimit;
		this.isLimit = isLimit;
		this.coordinateOrigin = coordinateOrigin;
		this.coordinateTarget = coordinateTarget;
		this.isAdvanced = isAdvanced;
	}

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{
				new Pawn(Color.WHITE), 
				Arrays.asList(new Pawn(Color.BLACK)), 0, new Coordinate[] {Coordinate.getInstance("61"), Coordinate.getInstance("52")}, null, 
				Coordinate.getInstance("18"), true, 
				Coordinate.getInstance("88"), Coordinate.getInstance("77"), true },
			{
				new Pawn(Color.BLACK), 
				Arrays.asList(new Pawn(Color.WHITE)), 0, new Coordinate[] {Coordinate.getInstance("53"), Coordinate.getInstance("61")}, Error.NOT_DIAGONAL, 
				Coordinate.getInstance("81"), true, 
				Coordinate.getInstance("11"), Coordinate.getInstance("22"), true },
			{
				new Pawn(Color.WHITE), 
				Arrays.asList(new Pawn(Color.WHITE)), 0, new Coordinate[] {Coordinate.getInstance("61"), Coordinate.getInstance("52")}, Error.COLLEAGUE_EATING, 
				Coordinate.getInstance("28"), false, 
				Coordinate.getInstance("11"), Coordinate.getInstance("33"), false },
			{
				new Pawn(Color.BLACK), 
				Arrays.asList(new Pawn(Color.WHITE)), 0, new Coordinate[] {Coordinate.getInstance("58"), Coordinate.getInstance("47")}, Error.NOT_ADVANCED, 
				Coordinate.getInstance("71"), false, 
				Coordinate.getInstance("44"), Coordinate.getInstance("33"), false },
			{
				new Pawn(Color.WHITE), 
				Arrays.asList(new Pawn(Color.BLACK)), 0, new Coordinate[] {Coordinate.getInstance("88"), Coordinate.getInstance("11")}, Error.TOO_MUCH_ADVANCED, 
				Coordinate.getInstance("28"), false, 
				Coordinate.getInstance("11"), Coordinate.getInstance("33"), false },
			{
				new Pawn(Color.WHITE), 
				Arrays.asList(), 0, new Coordinate[] {Coordinate.getInstance("61"), Coordinate.getInstance("43")}, Error.WITHOUT_EATING, 
				Coordinate.getInstance("28"), false, 
				Coordinate.getInstance("11"), Coordinate.getInstance("33"), false },
			{
				new Draught(Color.WHITE),
				Arrays.asList(new Pawn(Color.BLACK), new Pawn(Color.BLACK)), 0, new Coordinate[] {Coordinate.getInstance("88"), Coordinate.getInstance("11")}, Error.TOO_MUCH_EATINGS, 
				Coordinate.getInstance("28"), false, 
				Coordinate.getInstance("11"), Coordinate.getInstance("33"), false},
			{
				new Draught(Color.WHITE),
				Arrays.asList(new Pawn(Color.BLACK)), 0, new Coordinate[] {Coordinate.getInstance("88"), Coordinate.getInstance("11")}, null, 
				Coordinate.getInstance("28"), false, 
				Coordinate.getInstance("11"), Coordinate.getInstance("33"), false
			}
		});
	}

	@Test
	public void testIsLimit() {
		assertEquals(this.isLimit, this.piece.isLimit(coordinateLimit));
	}
	
	@Test
	public void testIsAdvanced() {
		assertEquals(this.isAdvanced, this.piece.isAdvanced(coordinateOrigin, coordinateTarget));
	}
	
	@Test
	public void testIsCorrectMovement() {
		assertEquals(expectedError, this.piece.isCorrectMovement(this.betweenDiagonalPieces, this.pair, this.coordinates));
	}
	
	@Test(expected = AssertionError.class)
	public void testGivenNullOriginWhenIsCorrectMovementThenThrowException() {
		this.piece.isAdvanced(null, this.coordinateTarget);
	}
	
	@Test(expected = AssertionError.class)
	public void testGivenNullTargetWhenIsCorrectMovementThenThrowException() {
		this.piece.isAdvanced(this.coordinateOrigin, null);
	}

}
