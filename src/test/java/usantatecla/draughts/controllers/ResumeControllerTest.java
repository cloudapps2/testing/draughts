package usantatecla.draughts.controllers;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import usantatecla.draughts.models.Game;
import usantatecla.draughts.models.State;

public class ResumeControllerTest {
	
	@InjectMocks
	private ResumeController resumeController;

	@Mock
	private Game game;

	@Mock
	private State state;

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void test() {
		this.resumeController.reset();
		verify(this.game).reset();
		verify(this.state).reset();
	}

}
