package usantatecla.draughts.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import usantatecla.draughts.models.Color;
import usantatecla.draughts.models.Coordinate;
import usantatecla.draughts.models.Error;
import usantatecla.draughts.models.Game;
import usantatecla.draughts.models.Piece;
import usantatecla.draughts.models.State;

public class PlayControllerTest {

    @InjectMocks
    private PlayController playController;

    @Mock
    private Game game;

    @Mock
    private State state;

    private Coordinate[] coordinatesWith2;
    private Coordinate[] coordinatesWith1;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        coordinatesWith1 = new Coordinate[1];
        coordinatesWith2 = new Coordinate[2];
        coordinatesWith1[0] = Coordinate.getInstance("11");
        coordinatesWith2[0] = Coordinate.getInstance("11");
        coordinatesWith2[1] = Coordinate.getInstance("22");

    }

    @Test
    public void testGiven2CoordinatesWhenMoveThenIsCorrect() {
        when(this.game.move(this.coordinatesWith2)).thenReturn(null);
        when(this.game.isBlocked()).thenReturn(false);

        this.playController.move(this.coordinatesWith2);

        verify(this.game).move(any(Coordinate.class));
        verify(this.game).isBlocked();
        verifyZeroInteractions(this.state);
    }

    @Test(expected = NullPointerException.class)
    public void testGivenNullCoordinatesWhenMoveThenThrowException() {
        this.playController.move(null);
    }

    @Test(expected = AssertionError.class)
    public void testGivenMoreThan2CoordinatesWhenMoveThenThrowException() {
        this.playController.move(this.coordinatesWith1);
    }

    @Test
    public void testGivenCoordinatesAndBadMovementWhenMoveThenReturnError() {
        when(this.game.move(this.coordinatesWith2)).thenReturn(Error.BAD_FORMAT);
        when(this.game.isBlocked()).thenReturn(false);

        this.playController.move(this.coordinatesWith2);

        verify(this.game).move(any(Coordinate.class));
        verify(this.game).isBlocked();
        verifyZeroInteractions(this.state);
    }

    @Test
    public void testGivenCoordinatesAndGameIsBlockedWhenMoveThenChangeState() {
        when(this.game.move(this.coordinatesWith2)).thenReturn(Error.BAD_FORMAT);
        when(this.game.isBlocked()).thenReturn(true);

        this.playController.move(this.coordinatesWith2);

        verify(this.game).move(any(Coordinate.class));
        verify(this.game).isBlocked();
        verify(this.state).next();
    }

    @Test
    public void testWhenCancel() {
        this.playController.cancel();

        verify(this.game).cancel();
        verify(this.state).next();
    }

    @Test
    public void testWhenGetColor() {
        when(this.game.getTurnColor()).thenReturn(Color.BLACK);

        this.playController.getColor();

        verify(this.game).getTurnColor();
    }

    @Test
    public void testWhenIsBlocked() {
        when(this.game.isBlocked()).thenReturn(false);

        this.playController.isBlocked();

        verify(this.game).isBlocked();
    }
    
    @Test
    public void testWhenGetPiece() {
    	when(this.game.getPiece(any(Coordinate.class))).thenReturn(any(Piece.class));
    	
    	this.playController.getPiece(Coordinate.getInstance("11"));
    	
    	verify(this.game).getPiece(any(Coordinate.class));
    }

}