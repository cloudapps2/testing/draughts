package usantatecla.draughts.controllers;

import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import usantatecla.draughts.models.Game;
import usantatecla.draughts.models.State;

public class StartControllerTest {

	@InjectMocks
	private StartController startController;

	@Mock
	private Game game;

	@Mock
	private State state;

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testWhenStart() {
		this.startController.start();

		verify(this.state).next();
	}

}
