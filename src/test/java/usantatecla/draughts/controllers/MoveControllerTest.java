package usantatecla.draughts.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import usantatecla.draughts.models.Color;
import usantatecla.draughts.models.Coordinate;
import usantatecla.draughts.models.Game;
import usantatecla.draughts.models.State;

public class MoveControllerTest {

    @InjectMocks
    private MoveController moveController;

    @Mock
    private Game game;

    @Mock
    private State state;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testWhenGetColor() {
        when(this.game.getColor(any(Coordinate.class))).thenReturn(Color.BLACK);

        Color color = this.moveController.getColor(Coordinate.getInstance("11"));

        verify(this.game).getColor(any(Coordinate.class));
        assertEquals(Color.BLACK, color);
    }
    
    @Test
    public void testWhenGetDimension() {
    	when(this.game.getDimension()).thenReturn(8);
    	
    	int dimersion = this.moveController.getDimension();
    	
    	verify(this.game).getDimension();
    	assertEquals(8, dimersion);
    }
}