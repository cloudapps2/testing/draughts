<h1 align='center'>Práctica de realización de Test para el juego de las Damas</h1>

<ul>
    <li>
        <a href="">Controllers</a>
        <ul>
            <li><a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/controllers/MoveControllerTest.java">MoveControllerTest</a></li>
            <li><a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/controllers/PlayControllerTest.java">PlayControllerTest</a></li>
            <li><a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/controllers/ResumeControllerTest.java">ResumeControllerTest</a></li>
            <li><a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/controllers/StartControllerTest.java">StartControllerTest</a></li>
        </ul>
    </li>
    <li>
        <a href="">Models</a>
        <ul>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/BoardTest.java">BoardTest</a>       
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/CoordinateTest.java">CoordinateTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/GameBuilder.java">GameBuilder</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/GameTest.java">GameTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/PieceParameterizedTest.java">PieceParameterizedTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/PieceTest.java">PieceTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/TurnTest.java">TurnTest</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="">Views</a>
        <ul>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/tree/master/src/test/java/usantatecla/draughts/views/AllViewsTest.java">AllViewsTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/tree/master/src/test/java/usantatecla/draughts/views/GameViewTest.java">GameViewTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/tree/master/src/test/java/usantatecla/draughts/views/PlayViewTest.java">PlayViewTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/tree/master/src/test/java/usantatecla/draughts/views/ResumeViewTest.java">ResumeViewTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/tree/master/src/test/java/usantatecla/draughts/views/StartViewTest.java">StartViewTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/tree/master/src/test/java/usantatecla/draughts/views/ViewTest.java">ViewTest</a>
            </li>
        </ul>
    </li>
</ul>

<h3>Coverage</h3>
<p>Cobertura de los tests de un 74%</p>

<h3>Uso</h3>
<p>mvn clean package</p>
java -jar target/draughts-1.0-SNAPSHOT.jar

<h3>Ejecución test</h3>
mvn test

<h3>Nota esperada: 5</h3>
<h5>Explicación:</h5>
<p>Aunque tenía experiencia previa en la realización de test, hay algunos conceptos que nunca llegué a utilizar. Aún me cuesta ver algunas
cosas y aunque siguiento las clases, he conseguido visualizar algunas y poder aplicarlas en mi código de test, aún me cuesta entenderlas o
saber en que momento utilizarlo. <b>Por ello, debido al esfuerzo relizado y las horas trabajadas</b> considero que mi nota es un <b>5</b>.
</p>

<h3>Notas</h3>
<ul>
    <li>
        <p>Al intentar realizar el test de la clase 
        <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/main/java/usantatecla/draughts/models/Piece.java">Piece</a>, 
        lo realicé sin parametrización. Depués de visualizar la clase del Martes día 10, conseguí entenderlo y decidí
        aplicarlo a mi código. Por ello tengo 2 test, uno previo a la realización de la parametrización y otro con parámetros</p>
        <ul>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/PieceParameterizedTest.java">PieceParameterizedTest</a>
            </li>
            <li>
                <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/models/PieceTest.java">PieceTest</a>
            </li>
        </ul>
    </li>
    <li>
        <p>Decidí hacer un test para la clase 
        <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/controllers/MoveControllerTest.java">MovementController</a>
         para poder testear los métodos de la clase padre 
         <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/main/java/usantatecla/draughts/controllers/Controller.java">Controller</a>. 
         Aunque pude testear la
        clase a partir de la realización del test de la clase 
        <a href="https://gitlab.com/cloudapps2/testing/draughts/-/blob/master/src/test/java/usantatecla/draughts/controllers/PlayControllerTest.java">PlayController</a>,
        necesitaba testear los métodos del controller desde otra, ya que
        sino tendría que haber refactorizado el código, y eso es algo que se deja para más adelante</p>
    </li>
</ul>

